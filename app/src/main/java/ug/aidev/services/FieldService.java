package ug.aidev.services;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.Header;
import retrofit.http.POST;
import ug.aidev.models.Field;
import ug.aidev.models.FieldRegistry;

/**
 * Created by william on 6/23/16.
 */
public interface FieldService {
    @POST("/v2/fields")
    void createField(@Header("Authorization") String authorization,
                     @Body FieldRegistry fieldRegistry, Callback<Field> fieldCB );
}
