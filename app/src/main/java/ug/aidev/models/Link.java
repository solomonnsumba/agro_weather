package ug.aidev.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by william on 6/23/16.
 */
public class Link implements Serializable{

    @SerializedName("self")
    private Href href;

    public String getLink(){
        return href.getLink();
    }

    public class Href{
        @SerializedName("href")
        private String link;

        public String getLink() {
            return link;
        }
    }
}
