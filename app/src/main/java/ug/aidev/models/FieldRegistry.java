package ug.aidev.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by william on 6/23/16.
 */
public class FieldRegistry implements Serializable{
    @SerializedName("farmId")
    private String farmId;
    @SerializedName("id")
    private String fieldId;
    @SerializedName("acres")
    private Integer acres;
    @SerializedName("name")
    private String fieldName;
    @SerializedName("centerPoint")
    private Point point;

    public FieldRegistry(String farmId, String fieldId, Integer acres, String fieldName, Point point) {
        this.farmId = farmId;
        this.fieldId = fieldId;
        this.acres = acres;
        this.fieldName = fieldName;
        this.point = point;
    }
}
